/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myinsights;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;

/**
 * FXML Controller class
 *
 * @author khangnhan
 */

   
public class SingleResultController extends MyInsights {
  
    @FXML
    private Button homeBtn;

    @FXML
    private Button exportBtn;

    @FXML
    private Button sandpitBtn;
    
    @FXML
    private Hyperlink alphaCohort;

    @FXML
    private Hyperlink betaCohort;

    @FXML
    private Hyperlink gammaCohort;


    @FXML
    void alphaConfirmation(ActionEvent event) {
        //change scene
        try{
            
            setScene("CohortConfirmation.fxml", "Cohort Confirmation");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }
    }

    @FXML
    void betaConfirmation(ActionEvent event) {
        //scene change
        try{
            
            setScene("CohortConfirmation.fxml", "Cohort Confirmation");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }
    }
    
    @FXML
    void gammaConfirmation(ActionEvent event) {
        //change scene
        try{
            
            setScene("CohortConfirmation.fxml", "Cohort Confirmation");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }
    }
    
    @FXML
    void exportDataBtn(ActionEvent event) {

    }

    @FXML
    void goHome(ActionEvent event) {
        //change scene
        try{
            
            setScene("Home.fxml", "Home");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }
    }

    @FXML
    void sandpitModeBtn(ActionEvent event) {
        //change scene
        try{
            
            setScene("Sandpit.fxml", "Sandpit Mode");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }
    }
    /**
     * Initializes the controller class.
     */
    
    public void initialize() {
        // TODO
    }    
    
}
