package myinsights;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

public class SandpitController extends MyInsights implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle rb){
        modeSelectionBar.getItems().addAll("Merge Datasets","General Statistics"); //Other options to be added/Greyed out
        
    }
    
    @FXML
    private Button homeBtn;

    @FXML
    private Button exportBtn;

    @FXML
    private Button backResults;

    @FXML
    private ComboBox<String> modeSelectionBar;

    @FXML
    private Button sandpitModeSelection;

    @FXML
    private Button currentDataBtn;

    @FXML
    void exportDataBtn(ActionEvent event) {

    }

    @FXML
    void goBack(ActionEvent event) {
       //FLAW - back button has to be able to go back where you came from!!
    }

    @FXML
    void goHome(ActionEvent event) {
        //change scene
        try{
            
            setScene("Home.fxml", "Home");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }
    }

    @FXML
    void selectMode(ActionEvent event) {
        //changes modes depending on what the user selects
        if (!modeSelectionBar.getSelectionModel().isEmpty()){
            String ModeSelect = modeSelectionBar.getSelectionModel().getSelectedItem().toString();
            if (ModeSelect == "Merge Datasets"){
                try{

                setScene("MergeInput.fxml", "Merge Input");

                } catch (Exception e){
                    System.out.println("error");
                    e.printStackTrace();

                    }   
            }
            
            if (ModeSelect == "General Statistics"){
                try{

                setScene("GeneralStats.fxml", "General Statistics");

                } catch (Exception e){
                    System.out.println("error");
                    e.printStackTrace();

                    }   
            }
        
        }
       
    }

    @FXML
    void selectModebar(ActionEvent event) {

    }

    @FXML
    void viewCurrentData(ActionEvent event) {

    }
    
    
}
