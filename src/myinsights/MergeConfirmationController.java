package myinsights;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class MergeConfirmationController extends MyInsights {

    @FXML
    private Button homeBtn;

    @FXML
    private Button continueBtn;

    @FXML
    private Button backBtn;

    @FXML
    private Button continueBtn1;

    @FXML
    private Button backBtn1;

    @FXML
    void dataConfirm(ActionEvent event) {

    }

    @FXML
    void goBack(ActionEvent event) {
        // changes scene
        try{
            
            setScene("MergeInput.fxml", "Merge Input");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }
    }

    @FXML
    void goHome(ActionEvent event) {
        //changes scene
        try{
            
            setScene("Home.fxml", "Home");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }
    }

}
