/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myinsights;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

/**
 * FXML Controller class
 *
 * @author khangnhan
 */
public class MultipleConfirmController extends MyInsights{

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private Button scYesBtn;

    @FXML
    private Button scNoBtn;

    @FXML
    void goBack(ActionEvent event) {
        //changes scene
        try{
            
            setScene("Home.fxml", "Home");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }
    }

    @FXML
    void goNext(ActionEvent event) {
        //changes scene
        try{
            
            setScene("MultipleResult.fxml", "Multiple Entry - Search Results");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }

    }
    
}
