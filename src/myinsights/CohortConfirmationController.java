/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myinsights;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author khangnhan
 */
public class CohortConfirmationController extends MyInsights {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private Button scNoBtn;

    @FXML
    private Button scYesBtn;
    
    @FXML
    private Label cohortId;

    @FXML
    private Label cohortNoRecords;


    @FXML
    void goBack(ActionEvent event) {
        //changes scene
        try{
            
            setScene("SingleResult.fxml", "Single Search Result");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }
    }

    @FXML
    void goNext(ActionEvent event) {
        //changes scene
        try{

                    setScene("MultipleResult.fxml", "Cohort Search Result");

                } catch (Exception e){
                    System.out.println("error");
                    e.printStackTrace();

                }
    }
    
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
