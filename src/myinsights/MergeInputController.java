/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myinsights;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;

/**
 * FXML Controller class
 *
 * @author khangnhan
 */
public class MergeInputController extends MyInsights implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // adds merge option for user, more to be added
        mergeType.getItems().addAll("Inner Merge","Other Merge");
        mergeType.getSelectionModel().selectFirst(); 
        
    }    
    
    
    @FXML
    private ComboBox<String> mergeType;
    
    @FXML
    private Button homeBtn;

    @FXML
    private Button continueBtn;

    @FXML
    private Button backBtn;

    @FXML
    private Button importBtn;

    @FXML
    void dataConfirm(ActionEvent event) {
        //changes scene
        try{
            
            setScene("MergeConfirmation.fxml", "Merge Confirmation");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }
    }

    @FXML
    void goBack(ActionEvent event) {
        //changes scene
        try{
            
            setScene("Sandpit.fxml", "Sandpit Mode");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }
    }

    @FXML
    void goHome(ActionEvent event) {
        //changes scene
        try{
            
            setScene("Home.fxml", "Home");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }
    }

    @FXML
    void importData(ActionEvent event) {
        //import function to be added
    }
    
    @FXML
    void mergeSelectionType(ActionEvent event) {
        //merge selection to be added

    }
    
}
