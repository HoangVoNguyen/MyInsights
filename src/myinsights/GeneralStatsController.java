/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myinsights;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class GeneralStatsController extends MyInsights{

    @FXML
    private Button backBtn;

    @FXML
    private Button continueBtn;

    @FXML
    private Button homeBtn;

    @FXML
    void goHome(ActionEvent event) {
        //changes scene
        try{
            
            setScene("Home.fxml", "Home");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }

    }

    @FXML
    void exportStats(ActionEvent event) {
        //exports function to be created
    }

    @FXML
    void goBack(ActionEvent event) {
        //changes scene
        try{
            
            setScene("Sandpit.fxml", "Sandpit");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }

    }

}
