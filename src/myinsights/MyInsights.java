/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myinsights;

import java.io.IOException;
import javafx.application.Application;
//import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
//import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author noosh_mbp
 */
public class MyInsights extends Application {

    private static Stage stage;

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Login.fxml"));
        
        MyInsights.stage = stage;
        
        stage.setTitle("My Insights - Login");
        Scene scene = new Scene(root);        
        stage.setScene(scene);
        stage.show();
    }
    public static Stage getPrimaryStage() {
        return stage;
    }

    private void setPrimaryStage(Stage pStage) {
        MyInsights.stage = pStage;
    }

    public void setScene(String fxmlFile, String stgtitle) throws IOException{
    //function to load and change scenes according to buttons being pressed
        System.out.println(fxmlFile);    
        Parent root = FXMLLoader.load(getClass().getResource(fxmlFile));
//        System.out.println("loaded fxml file");
        Scene scene = new Scene(root);
//        System.out.println("Created new scene called root");
        
        getPrimaryStage().setScene(scene);
        stage.setTitle("My Insights - " + stgtitle);
//        System.out.println("stage with scene");
        stage.show();
//        System.out.println("showing stage");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
