/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myinsights;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

/**
 * FXML Controller class
 *
 * @author khangnhan
 */
public class MultipleResultController extends MyInsights implements Initializable{

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    
    
    @FXML
    private Button sandpitBtn;
    
    @FXML
    void goSandpitMode(ActionEvent event) {
        //changes scene
        try{
            
            setScene("Sandpit.fxml", "Sandpit Mode");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }
    }
    
    @FXML
    void goHome(ActionEvent event) {
        //changes scene
        try{
            
            setScene("Home.fxml", "Home");
            
        } catch (Exception e){
            System.out.println("error");
            e.printStackTrace();
                    
        }
    }

    
    
}
