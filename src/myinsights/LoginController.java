package myinsights;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.*;
import myinsights.MyInsights;

public class LoginController extends MyInsights {

    @FXML
    private Button button;

    @FXML
    private PasswordField password;

    @FXML
    private Label label;

    @FXML
    private TextField username;
    
    int incorrectPasswordCount = 0;

    @FXML
    void loginButtonAction(ActionEvent event){

//        try{
//            
//            Parent homeViewParent = FXMLLoader.load(getClass().getResource("Home.fxml"));
//            Scene homeViewScene = new Scene(homeViewParent);
//            
//            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
//            window.setScene(homeViewScene);
//            window.show();
//            
//        } catch (Exception e){
//            System.out.println("error");
//            e.printStackTrace();
//                    
//        }

        if((username.getText().toString().equals("admin"))&&(password.getText().toString().equals("admin"))){
            try{
            
            setScene("Home.fxml", "Home");
            
            } catch (Exception e){
                System.out.println("error");
                e.printStackTrace();

            }
        }
        else{
            label.setVisible(true);
            incorrectPasswordCount++;
            if(incorrectPasswordCount == 3){
                button.setDisable(true);
                label.setText("Your account has been disabled. Please contact the administrator.");
            }
        }
        
        
          
        

    }

}
