package myinsights;

import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class HomeController extends MyInsights{

    @FXML
    private Label roleType;

    @FXML
    private Label idType;

    @FXML
    private ComboBox<?> roleTypeComboBox;

    @FXML
    private ComboBox<String> idTypeComboBox;

    @FXML
    private Label singleEntry;

    @FXML
    private Label multiEntry;

    @FXML
    private Label idNumberSingleEntry;

    @FXML
    private Label idNumberMultiEntry;

    @FXML
    private TextField idMultiEntryTextField;

    @FXML
    private TextField idSingleEntryTextField;

    @FXML
    private Button singleEntryBtn;

    @FXML
    private Button mutliEntryBtn;

    @FXML
    private Button searchMultiBtn;

    @FXML
    private Button importBtn;

    @FXML
    private Button removeBtn;

    @FXML
    private ListView<?> recentSearchListView;

    @FXML
    private Label recentSearches;

    @FXML
    private ListView<String> multiSearchListView;

    @FXML
    void searchIndv(ActionEvent event) {
        
        //checking to see if an ID Type has been selected
        if(!idTypeComboBox.getSelectionModel().isEmpty()){ 
            try{
                //Cohort searches need to go to a different screen
                if(idTypeComboBox.getSelectionModel().getSelectedItem().toString().equals("Cohort")){
                    setScene("CohortConfirmation.fxml", "Confirmation");
                }
                else{ //all other options leed to the 
                    setScene("SingleConfirm.fxml", "Confirmation");
                }
            
            } catch (Exception e){
                System.out.println("error");
                e.printStackTrace();
                    
            }
        }  
    }
    
    @FXML
    void searchMulti(ActionEvent event) {
         //scene changing
         if(!idTypeComboBox.getSelectionModel().isEmpty()){
            try{
            
                setScene("MultipleConfirm.fxml", "Confirmation");
            
            } catch (Exception e){
                System.out.println("error");
                e.printStackTrace();
                    
            }
        }
    }
    
    @FXML
    void addMultiEntry(ActionEvent event) {
        String idEntry = idMultiEntryTextField.getText().toString(); //attaches item from user into list
        if (idEntry != null && !idEntry.isEmpty()) {
            multiSearchListView.getItems().add(idEntry);
            removeBtn.setDisable(false);
            searchMultiBtn.setDisable(false);
        }
  
    }

    @FXML
    void removeMulti(ActionEvent event) { //removes item that was added
        if(multiSearchListView.getItems().size() == 1){
            removeBtn.setDisable(true);
            searchMultiBtn.setDisable(true);
        }
        int i = multiSearchListView.getSelectionModel().getSelectedIndex();
        multiSearchListView.getItems().remove(i);
    }

    @FXML
    void initialize(){ 
        idTypeComboBox.getItems().addAll("TFN","ABN","CIIN", "Cohort"); // adds options into a drop down box to allow user to choose ID type.
        removeBtn.setDisable(true);
        searchMultiBtn.setDisable(true);
    }
    
    
    
    

}
